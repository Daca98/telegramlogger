# from distutils.core import setup
import setuptools
from setuptools import setup

setup(name="telegramlogger",
      version="1.0.0",
      description="Telegram logger",
      license='MIT',
      author="Mattia Dalle",
      author_email="mat.daca@gmail.com",
      url='https://gitlab.com/Daca98/telegramlogger.git',
      packages=setuptools.find_packages()
      )
