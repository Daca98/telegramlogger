import os
import logging
import requests as requests

telegram_logging = logging.getLogger(name="telegram_logger")


class TelegramLogger:
    @staticmethod
    def send_message(message):
        params = {
            'parse_mode': 'Markdown',
            'text': message,
            'chat_id': os.environ.get('RECEIVER')
        }
        response = requests.get('https://api.telegram.org/bot{bot_token}/sendMessage'.format(
            bot_token=os.environ.get('TELEGRAM_API_TOKEN')),
            params=params)

        if os.environ.get('TELEGRAM_VERBOSE'):
            status_log = f'Telegram response status code {response.status_code}'
            response_log = f'Telegram response body {response.text}'
            if response.status_code == 200:
                telegram_logging.debug(f'{status_log}\n{response_log}')
            else:
                telegram_logging.error(f'{status_log}\n{response_log}')
        return response.status_code == 200
